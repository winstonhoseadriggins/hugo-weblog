+++
title = "Now"
+++

# What I'm doing now

- Working at [BA3](http://ba3.us) on our advanced mapping engine and helping clients integrate it. Lately, I've been focused on adding Mac support so that I can build another idea with our engine.

- After a delay of a few months, I'm back to actively organizing [CocoaHeads](https://meetup.com/nscoderrtp). This year already has some great talks scheduled.

- Working on a personal Mac project that I've been attempting to build for years. More on this later!

- On that note, also working on a personal web-based project I've been wanting to build for years so that I can learn [Elixir](http://elixir-lang.org), [Elm](http://elm-lang.org), and the [Phoenix Framework](http://www.phoenixframework.org). More on that later.

- Learning to Draw! After years of not doing film-making, I've been diving into drawing so that I can tell visual stories in comic book form. I have an idea for a small comic strip to start and am working on building out how do to that.

- Learning German for both my love of the language and a potential future trip to Germany. I'm currently trying to do at least one lesson on [Duolingo](http://duolingo.com)

- (This is a [now page](http://nownownow.com/about), last updated on January 18, 2017)
